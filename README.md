## Synopsis

Piboid - a PiBox media system controller for Android devices.

## Build

Piboid is built on a local Linux box using ant.  It does not use Maven or other higher-level build utilities.

### Local Build

To get a list of build targets, use the following command.

    ant -p

To build locally, use the following command.

    ant install

To build an instrumented application, used in debugging, use the following command.

    ant instrument

To build a release version use the following command.

    ant release

## Installation

Piboid must be installed to an Android device.  The install targets are based on the Android SDK.

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD
