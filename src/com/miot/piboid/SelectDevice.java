package com.miot.piboid;

// Java
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// Android
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.TransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class SelectDevice extends Activity
{
    final String TAG = "SelectDevice";
    final Context context = this;
    ListView list;
    private List<String> devices;
    SharedPreferences sharedPref;

    /*
     * -----------------------------------------
     * Setup
     * -----------------------------------------
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectdevice);

        // Load the configured devices
        sharedPref = 
            context.getSharedPreferences(getString(R.string.device_list), Context.MODE_PRIVATE);
        devices = new ArrayList<String>();
        Map<String,?> keys = sharedPref.getAll();
        if (keys.size() != 0 )
        {
            Log.i(TAG, "number of keys in " + R.string.device_list + ": " + keys.size());
            for(Map.Entry<String,?> entry : keys.entrySet())
            {
                String ipAddr = entry.getKey();
                String devName = (String)entry.getValue();
                if ( (devName != null) && (devName.length() > 0) )
                    devices.add( ipAddr + " / " + devName);
                else
                    devices.add( ipAddr );
            }
        }
        else
            Log.i(TAG, "number of keys in " + R.string.device_list + ": " + keys.size());
        list = (ListView)findViewById(R.id.selectdevice_listview);
        CreateListView();
    }

    private void CreateListView()
    {
        if (devices.size() == 0 )
        {
            Toast.makeText(SelectDevice.this, R.string.no_devices_cfg, Toast.LENGTH_LONG).show();
            Intent intent = new Intent();
            setResult(RESULT_CANCELED, intent);
            finish();
        }
        else
        {
            //Create an adapter for the listView and add the ArrayList to the adapter.
            list.setAdapter(
                new ArrayAdapter<String>(SelectDevice.this, android.R.layout.simple_list_item_1,devices));
            list.setOnItemClickListener( new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
                    {
                        //args2 is the listViews Selected index
                        final int idx = arg2;

                        // Load the layout for the device prompt into a dialog.
                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.device);
                        dialog.setTitle(R.string.title_device);
 
                        // set the custom dialog components - text and buttons
                        TextView text = (TextView) dialog.findViewById(R.id.remote_device);
                        String deviceStr = devices.get(idx);
                        text.setText(deviceStr);
                        String[] fields = deviceStr.split("/");
                        final String deviceIP = fields[0].trim();
 
                        Button connectButton = (Button) dialog.findViewById(R.id.connect_button);
                        Button deleteButton = (Button) dialog.findViewById(R.id.delete_button);
                        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

                        // Button actions
                        connectButton.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Login(deviceIP);
                                dialog.dismiss();
                            }
                        });
                        deleteButton.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.remove(deviceIP);
                                editor.commit();
                                Toast.makeText(SelectDevice.this, 
                                    "Removed " + deviceIP, Toast.LENGTH_SHORT).show();
                                devices.remove(idx);
                                ArrayAdapter aadapter = (ArrayAdapter)list.getAdapter();
                                aadapter.notifyDataSetChanged();
                                dialog.dismiss();
                                if (devices.size() == 0 )
                                    Toast.makeText(SelectDevice.this, 
                                        "No Devices Configured", Toast.LENGTH_LONG).show();
                            }
                        });
                        cancelButton.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }
                });
        }
    }

    /*
     * -----------------------------------------
     * Login dialog 
     * -----------------------------------------
     */
    public void Login(final String deviceIP) 
    {
        // Load the layout for the device prompt into a dialog.
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.login);
        dialog.setTitle(R.string.title_login);
 
        // Fill in cached values, if any.
        sharedPref = 
            context.getSharedPreferences(getString(R.string.credentials), Context.MODE_PRIVATE);
        String userid = sharedPref.getString(getString(R.string.hint_userid), "");
        String password = sharedPref.getString(getString(R.string.hint_pw), "");
        final EditText userIDWidget = (EditText) dialog.findViewById(R.id.login_userid);
        final EditText passwordWidget = (EditText) dialog.findViewById(R.id.login_pw);
        userIDWidget.setText(userid);
        passwordWidget.setText(password);

        Button connectButton = (Button) dialog.findViewById(R.id.Login_connect_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.Login_cancel_button);
        final ToggleButton toggleButton = (ToggleButton) dialog.findViewById(R.id.Login_togglepw_button);

        // Login attempts to login to the remote device.
        // If it succeeds, it goes back to the top level activity.
        // If it fails, it stays in the dialog.
        connectButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final String userid = userIDWidget.getText().toString();
                final String password = passwordWidget.getText().toString();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.hint_userid), userid);
                editor.putString(getString(R.string.hint_pw), password);
                if ( !editor.commit() )
                {
                    Log.e(TAG, "Failed to write userid/pw to file.");
                    Toast.makeText(SelectDevice.this, "Failed to save data", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Setup credentials for HTTP authenticated logins
                Authenticator.setDefault(new Authenticator(){
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(userid,password.toCharArray());
                    }
                });

                // Set the deviceIP for use in future messaging.
                Comm.setDevice(deviceIP);

                // Update the main activity's ActionBar with the connected device IP.
                Intent intent = new Intent();
                intent.putExtra(PiBoid.CURRENT_DEVICE, deviceIP);
                setResult(RESULT_OK, intent);
                finish();
                dialog.dismiss();
            }
        });

        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // Toggle button displays and hides the password.
        toggleButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( toggleButton.isChecked() )
                    passwordWidget.setTransformationMethod(null);
                else
                    passwordWidget.setTransformationMethod(new PasswordTransformationMethod());
                passwordWidget.setSelection(passwordWidget.length());
            }
        });
        dialog.show();
    }
}
