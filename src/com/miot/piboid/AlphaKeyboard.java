package com.miot.piboid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.os.Bundle;
import android.util.Log;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class AlphaKeyboard 
{
    final String TAG = "AlphaKeyboard";

    private Keyboard              alphaKeyboard;
    private CustomKeyboardView    alphaKeyboardView;
    private Activity              parent;

    // Keycodes returned from the keyboard
    public final static int CAPS         = -1; // Keyboard.KEYCODE_SHIFT
    public final static int CodeCancel   = -3; // Keyboard.KEYCODE_CANCEL
    public final static int CodeDelete   = -5; // Keyboard.KEYCODE_DELETE

    /*
     * -----------------------------------------
     * Key press handler
     * -----------------------------------------
     */
    private OnKeyboardActionListener alphaOnKeyboardActionListener = new OnKeyboardActionListener() {
        @Override public void onKey(int primaryCode, int[] keyCodes) {
            // Handle key
            if( primaryCode==CodeCancel )
                hideKbd();
            else if( primaryCode==CAPS )
                alphaKeyboardView.toggleCaps();
            else 
            {
                // Convert cap state if necessary
                if ( (primaryCode >= 97) && (primaryCode <=122) )
                    if ( alphaKeyboardView.getCapState() )
                        primaryCode -= 32;
                
                Comm.send( KeyCodes.get(KeyCodes.KEYBOARD_ALPHA, ""+primaryCode) );
            }
        }

        @Override public void onPress(int arg0) {
        }

        @Override public void onRelease(int primaryCode) {
        }

        @Override public void onText(CharSequence text) {
        }

        @Override public void swipeDown() {
        }

        @Override public void swipeLeft() {
        }

        @Override public void swipeRight() {
        }

        @Override public void swipeUp() {
        }
    };

    /*
     * -----------------------------------------
     * Constructor
     * -----------------------------------------
     */
    public AlphaKeyboard(Activity parent)
    {
        this.parent = parent;
        alphaKeyboard = new Keyboard(parent,R.xml.kbd_alpha);
        alphaKeyboardView = (CustomKeyboardView)parent.findViewById(R.id.alpha_keyboard);
        alphaKeyboardView.setKeyboard( alphaKeyboard );
        alphaKeyboardView.setPreviewEnabled(false);
        alphaKeyboardView.setOnKeyboardActionListener(alphaOnKeyboardActionListener);
    }

    /*
     * -----------------------------------------
     * Public methods
     * -----------------------------------------
     */
    public void hideKbd() {
        alphaKeyboardView.setVisibility(View.GONE);
        alphaKeyboardView.setEnabled(false);
    }

    public void showKbd() {
        alphaKeyboardView.setVisibility(View.VISIBLE);
        alphaKeyboardView.setEnabled(true);
    }

    public boolean isVisible() {
        return alphaKeyboardView.getVisibility() == View.VISIBLE;
    }
}
