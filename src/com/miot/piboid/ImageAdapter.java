package com.miot.piboid;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
	int cell_w, cell_h;

    public ImageAdapter(Context c, int cell_w, int cell_h) {
        mContext = c;
		this.cell_w = cell_w;
		this.cell_h = cell_h;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(cell_w-4, cell_w-2));
            // imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(2, 2, 2, 0);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.num, R.drawable.arrow_up, R.drawable.abc, 
            R.drawable.arrow_left, R.drawable.enter, R.drawable.arrow_right,
            R.drawable.home, R.drawable.arrow_down, R.drawable.pitab
    };
}
