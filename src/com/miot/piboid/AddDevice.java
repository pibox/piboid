package com.miot.piboid;

// Android
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddDevice extends Activity
{
    final Context context = this;
    final String TAG = "AddDevice";
    EditText editText;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adddevice);
        editText = (EditText)findViewById(R.id.adddevice_ipaddress);
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       android.text.Spanned dest, int dstart, int dend) 
            {
                if (end > start) 
                {
                    String destTxt = dest.toString();
                    String resultingTxt = 
                        destTxt.substring(0, dstart) + 
                        source.subSequence(start, end) + 
                        destTxt.substring(dend);
                    if (!resultingTxt.matches(
                            "^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) 
                    { 
                        return "";
                    } 
                    else
                    {
                        String[] splits = resultingTxt.split("\\.");
                        for (int i=0; i<splits.length; i++) 
                        {
                            if (Integer.valueOf(splits[i]) > 255)
                                return "";
                        }
                    }
                }
                return null;
            }
        };
        editText.setFilters(filters);
    }

    /*
     * Add a new device to the list of devices we know about.
     */
    public void newDevice(View view)
    {
        // Get and validate the IP address
        editText = (EditText)findViewById(R.id.adddevice_ipaddress);
        String deviceIP = editText.getText().toString();
        if ( deviceIP.split("\\.").length != 4 )
        {
            Toast.makeText(AddDevice.this, "Invalid IP address: " + deviceIP, Toast.LENGTH_SHORT).show();
            return;
        }
        editText = (EditText)findViewById(R.id.adddevice_devicename);
        String deviceName = editText.getText().toString();

        SharedPreferences sharedPref = 
            context.getSharedPreferences(getString(R.string.device_list), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(deviceIP, deviceName);
        if ( !editor.commit() )
        {
            Log.e(TAG, "Failed to write data to file.");
            Toast.makeText(AddDevice.this, "Failed adding " + deviceIP, Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(AddDevice.this, "Added " + deviceIP, Toast.LENGTH_SHORT).show();
    }
}
