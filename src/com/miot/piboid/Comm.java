package com.miot.piboid;

// Java
import java.io.*;
import java.net.*;
import java.util.*;

// Android
import android.os.AsyncTask;
import android.util.Log;

/*
 * Managed communications with the remote PiBox device.
 * Note that this communication is one-way.  We send data
 * to the device but receive nothing in return.
 */
public class Comm {

    private static final String TAG = "Comm";
    static private String deviceIP = null;

    /*
     * ---------------------------------------------------
     * Constructor
     * ---------------------------------------------------
     */
    public Comm() { }

    /*
     * -----------------------------------------
     * Inner classes
     * -----------------------------------------
     */
    static class SubmitKeyCode extends AsyncTask<String, Void, String> 
    {
        String line;
        String result = "";
        private Exception exception;

        // This is run in background thread.
        protected String doInBackground(String... data) {
            Log.i( TAG, "URL: " + data[0]);
            try {
                URL url = new URL( data[0] );
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setUseCaches(false);
                con.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((line = rd.readLine()) != null) {
                    result += line;
                }
                rd.close();
                Log.i( TAG, "Success for URL");
            }
            catch (Exception e) 
            {
                Log.e( TAG, "Send to " + data[1] + " failed: " + e.getMessage(), e);
                this.exception = e;
            }
            return null;
        }

        // Handled in main thread.
        protected void onPostExecute(String str) {
        }
    }

    /*
     * ---------------------------------------------------
     * Public Methods
     * ---------------------------------------------------
     */

    /*
     * Send a gdkkeysym name.  The remote device will map it to actions.
     */
    static public void setDevice(String ipAddr)
    {
        Log.i( TAG, "Setting device IP: " + ipAddr);
        deviceIP = ipAddr;
    }

    /*
     * Send a gdkkeysym name.  The remote device will map it to actions.
     */
    public static String send(String code)
    {
        Log.i( TAG, "Send entered: code = " + code);

        // Do nothing if we're not connected.
        if ( deviceIP == null )
        {
            Log.e( TAG, "Not connected.");
            return "Not Connected";
        }

        // We simply send it.  We don't wait for responses.
        String urlStr = "http://" + deviceIP + ":2001/php/device.php?c=/v1/piboid/" + code;
        new SubmitKeyCode().execute(urlStr, deviceIP);

        return null;
    }
}

