package com.miot.piboid;

// Java
import java.util.List;

// Android
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class CustomKeyboardView extends KeyboardView {

    Context context;
    boolean capsEnabled = false;
    final String TAG = "CustomKeyboardView";

    public CustomKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        List<Key> keys = getKeyboard().getKeys();
        for (Key key : keys) 
        {
            // Change the background color of the CAPS key when its enabled.
            if ( (key.codes[0] == AlphaKeyboard.CAPS) && capsEnabled )
            {
                Drawable dr = (Drawable) context.getResources().getDrawable(R.drawable.white_tint);
                dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
                dr.draw(canvas);
            }
        }
    }

    public void toggleCaps()
    {
        if ( capsEnabled )
            capsEnabled = false;
        else
            capsEnabled = true;

        Log.i(TAG, "CAPS state: " + capsEnabled);
    }

    public boolean getCapState()
    {
        return capsEnabled;
    }
}

