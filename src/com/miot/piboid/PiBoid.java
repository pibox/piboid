package com.miot.piboid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class PiBoid extends Activity
{
    final String TAG = "SelectDevice";
    public final static String CURRENT_DEVICE = "com.miot.piboid.CURRENT_DEVICE";
    final Context context = this;

    // Must match the layout setting in main.xml!
    public final static int NUM_COL      = 3; 

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        int displayWidth, displayHeight, statusbar_height, required_height;
        int cell_w, cell_h;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // Find the display size so we can fit the keypad.
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        displayWidth = metrics.widthPixels ;
        displayHeight = metrics.heightPixels;
        statusbar_height = getStatusBarHeight(getApplicationContext());
        required_height = displayHeight - statusbar_height ;

        // Compute cell sizes for keypad
        cell_w = displayWidth/NUM_COL;
        cell_h = required_height/NUM_COL;

        // The keypad 
        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(this, cell_w, cell_h));

        // Special keyboards displayed by their associated cells in the keypad.
        final NumPlusKeyboard numPlusKbd = new NumPlusKeyboard(this);
        final AlphaKeyboard alphaKbd = new AlphaKeyboard(this);

        gridview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                // Keypad actions
                switch(position)
                {
                    // Show Numeric/Special Keys keyboard
                    case 0:
                        numPlusKbd.showKbd();
                        alphaKbd.hideKbd();
                        break;

                    // Up
                    case 1:
                        Comm.send( KeyCodes.get(KeyCodes.KEYBOARD_KEYPAD, "Up") );
                        break;

                    // Show Alphabetic keyboard
                    case 2:
                        alphaKbd.showKbd();
                        numPlusKbd.hideKbd();
                        break;

                    // Left
                    case 3:
                        Comm.send( KeyCodes.get(KeyCodes.KEYBOARD_KEYPAD, "Left") );
                        break;

                    // Return/Enter
                    case 4:
                        Comm.send( KeyCodes.get(KeyCodes.KEYBOARD_KEYPAD, "Enter") );
                        break;

                    // Right
                    case 5:
                        Comm.send( KeyCodes.get(KeyCodes.KEYBOARD_KEYPAD, "Right") );
                        break;

                    // Home
                    case 6:
                        Comm.send( KeyCodes.get(KeyCodes.KEYBOARD_KEYPAD, "Home") );
                        break;

                    // Down
                    case 7:
                        Comm.send( KeyCodes.get(KeyCodes.KEYBOARD_KEYPAD, "Down") );
                        break;

                    // Tab
                    case 8:
                        Comm.send( KeyCodes.get(KeyCodes.KEYBOARD_KEYPAD, "Tab") );
                        break;

                }
            }
        });
    }

    /** Called when it becomes visible again. */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult() has been called.");
        if ( resultCode != RESULT_OK )
            return;
        String message = data.getStringExtra(CURRENT_DEVICE);
        Log.i(TAG, "Current Device: " + message);
        if ( (message != null) && (message.length() > 0) )
            getActionBar().setTitle(getString(R.string.app_name) + " to " + message);
        else
            getActionBar().setTitle(getString(R.string.app_name));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_selectdevice:
                intent = new Intent(context, SelectDevice.class);
                startActivityForResult(intent, 1);
                return true;
            case R.id.action_adddevice:
                intent = new Intent(context, AddDevice.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Find the height of the status bar, if displayed.
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) 
            result = context.getResources().getDimensionPixelSize(resourceId);
        return result;
    }
}
