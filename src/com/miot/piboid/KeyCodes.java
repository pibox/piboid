package com.miot.piboid;

// Java
import java.io.*;
import java.net.*;
import java.util.*;

// Android
import android.os.AsyncTask;
import android.util.Log;

/*
 * Class that handles conversion of keypad/keyboard
 * keycodes to GDK keysyms to send to PiBox device.
 */
public class KeyCodes {

    public static final int KEYBOARD_KEYPAD     = 1;
    public static final int KEYBOARD_ALPHA      = 2;
    public static final int KEYBOARD_NUM        = 3;

    private static final Map<String, String> KEYPAD;
    private static final Map<String, String> ALPHA;
    private static final Map<String, String> NUMERIC;

    /*
     * ---------------------------------------------------
     * Static initialization of the HashMap's.
     * ---------------------------------------------------
     */
    static {

        KEYPAD = new HashMap<String, String>();
        KEYPAD.put("up",    "KP_Up");
        KEYPAD.put("left",  "KP_Left");
        KEYPAD.put("right", "KP_Right");
        KEYPAD.put("down",  "KP_Down");
        KEYPAD.put("enter", "Enter");
        KEYPAD.put("home",  "Esc");
        KEYPAD.put("tab",   "Tab");

        NUMERIC = new HashMap<String, String>();
        NUMERIC.put("48",   "0");
        NUMERIC.put("49",   "1");
        NUMERIC.put("50",   "2");
        NUMERIC.put("51",   "3");
        NUMERIC.put("52",   "4");
        NUMERIC.put("53",   "5");
        NUMERIC.put("54",   "6");
        NUMERIC.put("55",   "7");
        NUMERIC.put("56",   "8");
        NUMERIC.put("57",   "9");

        NUMERIC.put("33",   "exclam");
        NUMERIC.put("64",   "at");
        NUMERIC.put("35",   "numbersign");
        NUMERIC.put("36",   "dollar");
        NUMERIC.put("37",   "percent");
        NUMERIC.put("94",   "caret");
        NUMERIC.put("42",   "asterisk");
        NUMERIC.put("45",   "minus");
        NUMERIC.put("95",   "underscore");
        NUMERIC.put("32",   "space");
        NUMERIC.put("8",    "Backspace"); 

        ALPHA = new HashMap<String, String>();
        ALPHA.put("65",     "A"); ALPHA.put("97",     "a");
        ALPHA.put("66",     "B"); ALPHA.put("98",     "b");
        ALPHA.put("67",     "C"); ALPHA.put("99",     "c");
        ALPHA.put("68",     "D"); ALPHA.put("100",    "d");
        ALPHA.put("69",     "E"); ALPHA.put("101",    "e");
        ALPHA.put("70",     "F"); ALPHA.put("102",    "f");
        ALPHA.put("71",     "G"); ALPHA.put("103",    "g");
        ALPHA.put("72",     "H"); ALPHA.put("104",    "h");
        ALPHA.put("73",     "I"); ALPHA.put("105",    "i");
        ALPHA.put("74",     "J"); ALPHA.put("106",    "j");
        ALPHA.put("75",     "K"); ALPHA.put("107",    "k");
        ALPHA.put("76",     "L"); ALPHA.put("108",    "l");
        ALPHA.put("77",     "M"); ALPHA.put("109",    "m");
        ALPHA.put("78",     "N"); ALPHA.put("110",    "n");
        ALPHA.put("79",     "O"); ALPHA.put("111",    "o");
        ALPHA.put("80",     "P"); ALPHA.put("112",    "p");
        ALPHA.put("81",     "Q"); ALPHA.put("113",    "q");
        ALPHA.put("82",     "R"); ALPHA.put("114",    "r");
        ALPHA.put("83",     "S"); ALPHA.put("115",    "s");
        ALPHA.put("84",     "T"); ALPHA.put("116",    "t");
        ALPHA.put("85",     "U"); ALPHA.put("117",    "u");
        ALPHA.put("86",     "V"); ALPHA.put("118",    "v");
        ALPHA.put("87",     "W"); ALPHA.put("119",    "w");
        ALPHA.put("88",     "X"); ALPHA.put("120",    "x");
        ALPHA.put("89",     "Y"); ALPHA.put("121",    "y");
        ALPHA.put("90",     "Z"); ALPHA.put("122",    "z");
        ALPHA.put("8",      "Backspace"); 
    }

    /*
     * ---------------------------------------------------
     * Constructor
     * ---------------------------------------------------
     */
    public KeyCodes() { }

    /*
     * ---------------------------------------------------
     * Private Methods
     * ---------------------------------------------------
     */

    private static String getKeypad(String keyCode)
    {
        return KEYPAD.get(keyCode.toLowerCase());
    }

    private static String getAlpha(String keyCode)
    {
        return ALPHA.get(keyCode);
    }

    private static String getNum(String keyCode)
    {
        return NUMERIC.get(keyCode);
    }

    /*
     * ---------------------------------------------------
     * Public Methods
     * ---------------------------------------------------
     */

    /*
     * Get a gdkkeysym name based on which keyboard is being handled.  
     */
    public static String get(int keyboard, String keyCode)
    {
        switch (keyboard)
        {
            case KEYBOARD_KEYPAD:
                return getKeypad(keyCode);

            case KEYBOARD_ALPHA:
                return getAlpha(keyCode);

            case KEYBOARD_NUM:
                return getNum(keyCode);
        }

        // Should never get here!
        return null;
    }
}

