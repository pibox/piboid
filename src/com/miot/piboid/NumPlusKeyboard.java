package com.miot.piboid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.os.Bundle;
import android.util.Log;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class NumPlusKeyboard 
{
    final String TAG = "NumPlusKeyboard";

    private Keyboard        numplusKeyboard;
    private KeyboardView    numplusKeyboardView;
    private Activity        parent;

    // Keycodes returned from the keyboard
    public final static int CodeCancel   = -3; // Keyboard.KEYCODE_CANCEL

    /*
     * -----------------------------------------
     * Key press handler
     * -----------------------------------------
     */
    private OnKeyboardActionListener numplusOnKeyboardActionListener = new OnKeyboardActionListener() {
        @Override public void onKey(int primaryCode, int[] keyCodes) {
            // Handle key
            if( primaryCode==CodeCancel )
                hideKbd();
            else 
                Comm.send( KeyCodes.get(KeyCodes.KEYBOARD_NUM, ""+primaryCode) );
        }

        @Override public void onPress(int arg0) {
        }

        @Override public void onRelease(int primaryCode) {
        }

        @Override public void onText(CharSequence text) {
        }

        @Override public void swipeDown() {
        }

        @Override public void swipeLeft() {
        }

        @Override public void swipeRight() {
        }

        @Override public void swipeUp() {
        }
    };

    /*
     * -----------------------------------------
     * Constructor
     * -----------------------------------------
     */
    public NumPlusKeyboard(Activity parent)
    {
        this.parent = parent;
        numplusKeyboard = new Keyboard(parent,R.xml.kbd_numpluskeys);
        numplusKeyboardView = (KeyboardView)parent.findViewById(R.id.num_keyboard);
        numplusKeyboardView.setKeyboard( numplusKeyboard );
        numplusKeyboardView.setPreviewEnabled(false);
        numplusKeyboardView.setOnKeyboardActionListener(numplusOnKeyboardActionListener);
    }

    /*
     * -----------------------------------------
     * Public methods
     * -----------------------------------------
     */
    public void hideKbd() {
        numplusKeyboardView.setVisibility(View.GONE);
        numplusKeyboardView.setEnabled(false);
    }

    public void showKbd() {
        numplusKeyboardView.setVisibility(View.VISIBLE);
        numplusKeyboardView.setEnabled(true);
    }

    public boolean isVisible() {
        return numplusKeyboardView.getVisibility() == View.VISIBLE;
    }
}
